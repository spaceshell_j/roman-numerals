
const RomanNumerals = number => {
  const numeralsMap = ['M','CM','D','CD','C','XC','L','XL','X','IX','V','IV','I']
  const numbersMap = [1000,900,500,400,100,90,50,40,10,9,5,4,1]

  const toRoman = (num = number) => {
    let roman = ''
    while(num){
      let index = numbersMap.findIndex(val => val <= num)
      num -= numbersMap[index]
      roman = roman.concat(numeralsMap[index])
    }
    return roman
  }

  return number > 0 ? number < 4e3 ? toRoman()
    : console.log('Number too large!')
    : console.log('Number too Small!')
}

export default RomanNumerals
