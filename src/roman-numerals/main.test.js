import RomanNumerals from './main.js'

const dataPass = [234,45,756,234,1567,457,5,1247,24,2756,1796,45,756,3568]
const dataFail = [0,44565,75556,23890804,1456567,8908,89080,1890247,6767,345345,4000]

describe('Adding method', () => {
  test('Should produce numerals...', () => {
    for(let val of dataPass){
      const trial = RomanNumerals(val)
      expect(String(trial).length)
        .toBeGreaterThanOrEqual(String(val).length)
      expect(typeof trial)
        .toBe('string')
    }
  })

  test('Should not produce numerals...', () => {
    for(let val of dataFail){
      const trial = RomanNumerals(val)
      expect(typeof trial)
        .toBe('undefined')
    }
  })
})
